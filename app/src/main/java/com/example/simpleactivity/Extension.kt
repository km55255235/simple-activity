package com.example.simpleactivity

import android.app.Activity
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

fun Activity.goToActivity(nextActivity: Activity, bundle: Bundle? = null) {
    val intent = Intent(this, nextActivity::class.java)
    if (bundle != null){
        intent.putExtras(bundle)
    }
    startActivity(intent)
}

fun Activity.goToMaps(url: String){
    try {
        //cek paket content
    }catch (e: Exception){
        val intent = Intent(ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }
}

fun FragmentActivity.replaceFragment(resId: Int, replacebleFragment: Fragment){
    this.supportFragmentManager.beginTransaction()
        .replace(resId, replacebleFragment)
        .commit()
}