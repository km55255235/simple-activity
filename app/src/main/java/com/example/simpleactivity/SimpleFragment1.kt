package com.example.simpleactivity

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager

class SimpleFragment1 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.e("SimpleFragment1", "onCreateView")
        return inflater.inflate(R.layout.fragment_simple1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("SimpleFragment1", "onViewCreated")
    }
}

fun SimpleFragment1.show(resId: Int){
    childFragmentManager.beginTransaction()
        .replace(resId,this).commit()
}