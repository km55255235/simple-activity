package com.example.simpleactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val text = findViewById<TextView>(R.id.text2)
        val person = intent.extras?.getSerializable("CLASS") as Person
        val umur = intent.extras?.getInt(MainActivity.KEY_NAME, 0)
        text.setText(person.firstName)

        //button 1 diklik muncul fragment 1
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()

        val fragment1 = SimpleFragment1()
        ft.replace(R.id.container_fragment, fragment1)
        ft.commit()
        //button 2 diklik muncul fragment 2
        val fragment2 = SimpleFragment2()
        ft.hide(fragment1).show(fragment2)
        ft.commit()
    }

}