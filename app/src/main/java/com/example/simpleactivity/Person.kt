package com.example.simpleactivity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

data class Person(
    val firstName: String
) : Serializable
