package com.example.simpleactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.e("InisialisasiState", "onCreate")

        val name = "Arif"
        val lastname = "Rahman"
        val button = findViewById<Button>(R.id.button1)
        val text = findViewById<TextView>(R.id.text1)

        text.setText(name)
        val bundle = Bundle()
        bundle.putString(KEY_NAME, name)
        bundle.putString(LAST_NAME, lastname)
        bundle.putInt("UMUR", 20)

        val person = Person(name)
        bundle.putSerializable("CLASS", person)
        button.setOnClickListener {
            goToActivity(SecondActivity(), bundle)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("InisialisasiState", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("InisialisasiState", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("InisialisasiState", "onPause")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e("InisialisasiState", "onRestart")
    }

    override fun onStop() {
        super.onStop()
        Log.e("InisialisasiState", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("InisialisasiState", "onDestroy")
    }

    companion object {
        const val KEY_NAME = "NAME"
        const val LAST_NAME = "LAST_NAME"
    }
}